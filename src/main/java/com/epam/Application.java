package com.epam;


import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.BusinessLogic;
import com.epam.model.Model;
import com.epam.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        Model model = new BusinessLogic();
        Controller controller = new ControllerImpl(model);
        new MyView(controller).show();

    }
}


