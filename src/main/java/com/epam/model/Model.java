package com.epam.model;

public interface Model {
    void task0CustomContainer();

    void task0CustomPriorityQueue();

    void task0AddStringsToListIntegers();

    void task1ALogicTask();

    void task1BLogicTask();

    void task1CLogicTask();

    void task1DGame();

    void task2();

    void task3();

    void task4();

    void exit();
}
