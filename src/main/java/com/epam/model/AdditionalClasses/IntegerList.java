package com.epam.model.AdditionalClasses;

import java.util.ArrayList;
import java.util.List;

import static com.epam.Application.logger;

public class IntegerList {

        public static void addToList(List list) {
            list.add("0067");
            list.add("bb");
        }
        public static void testList() {
            List<Integer> list = new ArrayList<>();
            addToList(list);
            logger.info(list.get(0));
        }
}
