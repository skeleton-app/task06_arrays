package com.epam.model.AdditionalClasses;

import java.util.Arrays;

import static com.epam.Application.logger;

public class TaskA<T> {
    private static int thirdArrayLengthCounter = 0;
    private T[] firstArray ;
    private T[] secondArray ;
    private T[] thirdArray;

    public TaskA() {
        firstArray = (T[]) new Object[10];
        secondArray = (T[]) new Object[10];
        firstArray[0]=(T)"one";
        firstArray[1]=(T)"two";
        firstArray[2]=(T)"three";
        firstArray[3]=(T)"four";
        firstArray[4]=(T)"five";
        firstArray[5]=(T)"six";
        firstArray[6]=(T)"seven";
        firstArray[7]=(T)"eight";
        firstArray[8]=(T)"nine";
        firstArray[9]=(T)"ten";
        secondArray[0]=(T)"one";
        secondArray[1]=(T)"two";
        secondArray[2]=(T)"four";
        secondArray[3]=(T)"eight";
        secondArray[4]=(T)"sixteen";
        secondArray[5]=(T)"thirtyTwo";
        secondArray[6]=(T)"sixtyFour";
        secondArray[7]=(T)"oneHundredAndTwentyEight";
        secondArray[8]=(T)"twoHundredsAndFiftySix";
        secondArray[9]=(T)"fiveHundredsAndTwelve";
        logger.info("Arrays are initialized");
        logger.info(firstArray[3]);
        logger.info(secondArray[5]);
        thirdArray = (T[]) new Object[1];
        for (int i = 0; i < firstArray.length-1; i++) {
            for (int j = 0; j < secondArray.length-1; j++) {
                for (int a = 0; a < thirdArray.length-1; a++) {
                    if (firstArray[i].equals(secondArray[j])) {
                        if (!firstArray[i].equals(thirdArray[a])) {
                            thirdArrayLengthCounter++;
                            thirdArray = (T[]) new Object[thirdArrayLengthCounter+1];
                            thirdArray[thirdArrayLengthCounter] = firstArray[i];
                        }
                    }
                }
            }
        }
    }

    public void toStringTaskAa() {
        for(int i=0;i<thirdArray.length-1;i++){
            logger.info(thirdArray[i]);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskA<?> taskA = (TaskA<?>) o;
        return Arrays.equals(firstArray, taskA.firstArray) &&
                Arrays.equals(secondArray, taskA.secondArray) &&
                Arrays.equals(thirdArray, taskA.thirdArray);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(firstArray);
        result = 31 * result + Arrays.hashCode(secondArray);
        result = 31 * result + Arrays.hashCode(thirdArray);
        return result;
    }

    public void testTaskAa() {

    }

    public T testTaskAb() {
        return null;
    }

}
