package com.epam.model.AdditionalClasses;

import static com.epam.Application.logger;

public class Game {
    int rooms = 10;
    int behindTheDoorNumber;
    behindTheDoor surprise;
    Hero hero = new Hero();
    Monster monster;

    public enum behindTheDoor {
        ARTIFACT(1),
        MONSTER(0);
        int whoBehindTheDoor;

        behindTheDoor(int indentef) {
            this.whoBehindTheDoor = indentef;
        }
    }
    private void printCongratulation(int i){
        if(i==(rooms-1)){
            logger.info("###################################");
            logger.info("###   YOU WIN CONGRATULATIONS  ####");
            logger.info("###################################");
        }
    }
    public void playGame() {
        for (int i = 0; i < rooms; i++) {
            behindTheDoorNumber = (int) (Math.random() * 2);
            surprise = (behindTheDoorNumber == 1 ? behindTheDoor.ARTIFACT : behindTheDoor.MONSTER);
            logger.info("BehindTheDoor is " + surprise);
            if (surprise == behindTheDoor.ARTIFACT) {
                Artefact artefact = new Artefact();
                int artifactPower = artefact.getArtefact();
                logger.info("Artifact gives hero +" + artifactPower);
                hero.setPower((hero.getPower()+artifactPower));
                logger.info("Current hero power - " + hero.getPower());
                printCongratulation(i);
            } else {
                Monster monster = new Monster();
                logger.info("Monster has "+monster.getPower()+" power points");
                if ((hero.getPower() - monster.getPower()) <= hero.DEAD_HERO) {
                    logger.info("Game OVER");
                    logger.info("Monster WIN");
                    break;
                } else {
                    logger.info("Monster dead");
                    hero.setPower(hero.getPower() - monster.getPower());
                    logger.info("Current hero power - " + hero.getPower());
                    printCongratulation(i);
                }
            }
        }
    }

    class Hero {
        private int power = 25;
        public final int DEAD_HERO = 0;

        public void setPower(int power) {
            this.power = power;
        }

        public int getPower() {
            return power;
        }
    }

    class Monster {
        private int power = 15;
        private static final int MAX_POWER = 100;
        private static final int MIN_POWER = 5;

        public Monster() {
            power = (int) ((Math.random() * ((MAX_POWER - MIN_POWER) + 1)) + MIN_POWER);
        }

        public int getPower() {
            return power;
        }
    }

    class Artefact {
        private int power = 0;
        private static final int MAX_POWER = 80;
        private static final int MIN_POWER = 10;

        public Artefact() {
            power = (int) ((Math.random() * ((MAX_POWER - MIN_POWER) + 1)) + MIN_POWER);
        }

        public int getArtefact() {
            return power;
        }
    }
}
