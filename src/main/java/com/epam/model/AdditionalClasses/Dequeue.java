package com.epam.model.AdditionalClasses;

import static com.epam.Application.logger;

public class Dequeue {
    static final int MAX = 100;
    int  arr[];
    int  front;
    int  rear;
    int  size;

    public Dequeue(int size)
    {
        arr = new int[MAX];
        front = -1;
        rear = 0;
        this.size = size;
    }
    public boolean isFull()
    {
        return ((front == 0 && rear == size-1)||
                front == rear+1);
    }
    public boolean isEmpty ()
    {
        return (front == -1);
    }
    public void offerFirst(int key)
    {
        if (isFull())
        {
            logger.info("Overflow");
            return;
        }
        if (front == -1)
        {
            front = 0;
            rear = 0;
        }
        else if (front == 0)
            front = size - 1 ;
        else
            front = front-1;
        arr[front] = key ;
    }
    public void offerLast(int key)
    {
        if (isFull())
        {
            logger.info(" Overflow ");
            return;
        }
        if (front == -1)
        {
            front = 0;
            rear = 0;
        }
        else if (rear == size-1)
            rear = 0;
        else
            rear = rear+1;
        arr[rear] = key ;
    }
    public void pollFirst()
    {
        if (isEmpty())
        {
            logger.info("Queue Underflow\n");
            return ;
        }
        if (front == rear)
        {
            front = -1;
            rear = -1;
        }
        else
            if (front == size -1)
                front = 0;
            else
                front = front+1;
    }
    public void pollLast()
    {
        if (isEmpty())
        {
            logger.info(" Underflow");
            return ;
        }
        if (front == rear)
        {
            front = -1;
            rear = -1;
        }
        else if (rear == 0)
            rear = size-1;
        else
            rear = rear-1;
    }
    public int peekFirst()
    {
        if (isEmpty())
        {
            logger.info(" Underflow");
            return -1 ;
        }
        return arr[front];
    }
    public int peekLast()
    {
        if(isEmpty() || rear < 0)
        {
            logger.info(" Underflow\n");
            return -1 ;
        }
        return arr[rear];
    }
}
