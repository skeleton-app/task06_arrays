package com.epam.model.AdditionalClasses;

import java.util.HashMap;

import java.util.Map;

import static com.epam.Application.logger;

public class TaskB {
    private int[] array;
    String arr="";
    String result="";

    public TaskB() {
        array = new int[]{1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 9, 9, 10};
        for (int i = 0; i < array.length; i++) {
            arr = arr + (array[i] + " ");
        }
        logger.info("Input Array: " + arr);
    }

    void RemoveElements(int arr[], int n, int k) {
        Map<Integer, Integer> mp = new HashMap<>();
        for (int i = 0; i < n; ++i) {
            mp.put(arr[i], mp.get(arr[i]) == null ? 1 : mp.get(arr[i]) + 1);
        }
        for (int i = 0; i < n; ++i) {
            if (mp.containsKey(arr[i]) && mp.get(arr[i]) <= k) {

                result = result + (arr[i] + " ");

            }
        }
        logger.info("Output Array: " + result);
    }

    public void testTask() {
        int n = array.length;
        int k = 2;
        RemoveElements(array, n, k);
    }
}
