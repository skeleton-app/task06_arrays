package com.epam.model.AdditionalClasses;

public class Task2Container {
    String[] stringArray;
    int sizeOfArray;
    int elementsInArray;

    public Task2Container() {
        stringArray = new String[1];
        sizeOfArray = stringArray.length;
        elementsInArray = 0;
    }

    private void changeArraySize(int setSize) {
        sizeOfArray = setSize;
        String[] temp = stringArray;
        stringArray = new String[setSize];
        stringArray = temp;
    }

    private void add(String string) {
        if (sizeOfArray <= elementsInArray) {
            changeArraySize(elementsInArray);
            stringArray[elementsInArray] = string;
            elementsInArray++;
        } else {
            stringArray[elementsInArray] = string;
            elementsInArray++;
        }
    }

}
