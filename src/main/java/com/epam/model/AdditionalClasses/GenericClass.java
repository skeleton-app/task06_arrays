package com.epam.model.AdditionalClasses;

import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;

public class GenericClass<Object> {
    private List<Object> listObjects;
    private int element;
    private Logger logger;

    public GenericClass(Logger log) {
        listObjects = new LinkedList<>();
        logger = log;
        element = listObjects.size();
        logger.info(element);
    }

    public void put(Object object) {
        try {
            listObjects.add(object);
            element = listObjects.size();
            logger.info(element);
        } catch (ExceptionInInitializerError e) {
            logger.error(e);
        }
    }

    public void delete(Object object) {
        try {
            listObjects.remove(object);
            element = listObjects.size();
        } catch (NullPointerException e) {
            logger.error(e);
        }
    }

    public boolean getLast() {
        logger.info(element);
        if (listObjects != null) {
            listObjects.get(element - 1);
            return true;
        } else {
            return false;
        }
    }

}
