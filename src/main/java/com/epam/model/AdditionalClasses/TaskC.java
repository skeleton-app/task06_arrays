package com.epam.model.AdditionalClasses;

import static com.epam.Application.logger;

public class TaskC {
    private int[] array;
    String arr = "";
    String result = "";

    public TaskC() {
        array = new int[]{1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 9, 9, 10};
        for (int i = 0; i < array.length; i++) {
            arr = arr + (array[i] + " ");
        }
        logger.info("Input Array: " + arr);
    }

    void RemoveElements(int arr[], int n) {
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] == arr[i + 1]) {
                for (int j = i; j < n - 1; j++) {
                    arr[j] = arr[j + 1];
                }
            }
        }
    }

    private void printResult(int[] arr, int n) {
        for (int i = 0; i < n; ++i) {
            result = result + (arr[i] + " ");
        }
        logger.info("Output Array: " + result);
    }

    public void testTask() {
        int n = array.length;
        int counterOfElementsInNewArray=1;
        for (int i = 0; i < n - 1; ++i) {
            if (array[i] == array[i + 1]) {
                RemoveElements(array, n);
            }
        }
        for (int i = 0; i < n - 1; ++i) {
            if (array[i] != array[i + 1]) {
                counterOfElementsInNewArray++;
            }else break;
        }
        int[] newArray = new int[counterOfElementsInNewArray];
        for (int i = 0; i < counterOfElementsInNewArray; i++) {
           newArray[i] = array[i];
        }
        printResult(newArray,counterOfElementsInNewArray);
    }
}
