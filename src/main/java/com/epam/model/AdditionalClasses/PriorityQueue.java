package com.epam.model.AdditionalClasses;

import java.util.AbstractQueue;

import java.util.Iterator;

import static com.epam.Application.logger;

public class PriorityQueue<T> extends AbstractQueue<T> {

    private T[] arr;

    private int total, first, next;

    public PriorityQueue() {
        arr = (T[]) new Object[2];
    }

    private void resize(int capacity) {
        T[] tmp = (T[]) new Object[capacity];

        for (int i = 0; i < total; i++)
            tmp[i] = arr[(first + i) % arr.length];

        arr = tmp;
        first = 0;
        next = total;
    }

    private void makeSmaller(int capacity) {
        T[] tmp = (T[]) new Object[capacity];
        for (int i = 0; i < capacity; i++) {
            tmp[i] = arr[i + 1];
        }
        arr = tmp;
        first = 0;
        next = total;
    }

    public boolean add(T element) {
        try {
            if (arr.length == total) {
                resize(total + 1);
            }
            arr[next++] = element;
            if (next == arr.length) {
                next = 0;
            }
            total++;
            return true;
        } catch (Exception e) {
            logger.warn(e);
            return false;
        }
    }

    public T delete() {
        if (total == 0) throw new java.util.NoSuchElementException();
        T element = arr[0];
        makeSmaller((total - 1));
        if (++first == arr.length) {
            first = 0;
        }
        if (--total > 0 && total == arr.length / 4) {
            resize(arr.length / 2);
        }
        return element;
    }

    @Override
    public String toString() {
        return java.util.Arrays.toString(arr);
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public int size() {
        return arr.length;
    }

    @Override
    public boolean offer(T element) {
        if (arr.length == total) {
            return false;
        }
        arr[next++] = element;
        if (next == arr.length) {
            next = 0;
        }
        total++;
        return true;
    }

    @Override
    public T poll() {
        return delete();
    }

    @Override
    public T peek() {
        return arr[first];
    }
}
