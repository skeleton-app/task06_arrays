package com.epam.model;

import com.epam.model.AdditionalClasses.*;

import static com.epam.Application.logger;

public class BusinessLogic implements Model {

    @Override
    public void task0CustomContainer() {
        GenericClass<String> books = new GenericClass<>(logger);
        logger.info("Book1");
        books.put("Book1");
        logger.info("Book2");
        books.put("Book2");
        logger.info("Book3");
        books.put("Book3");
        logger.info("Last book: " + books.getLast());
        books.delete("Book3");
        logger.info("Delete book3: " + books.getLast());
        books.delete("Book1");
        logger.info("Delete book1: " + books.getLast());
    }

    @Override
    public void task0CustomPriorityQueue() {
        PriorityQueue<String> priorityQueue = new PriorityQueue<>();
        logger.info("priorityQueue.add(\"Monday\");");
        priorityQueue.add("Monday");
        logger.info("priorityQueue.add(\"Tuesday\");");
        priorityQueue.add("Tuesday");
        logger.info("priorityQueue.add(\"Wednesday\");");
        priorityQueue.add("Wednesday");
        logger.info("priorityQueue.add(\"Thursday\");");
        priorityQueue.add("Thursday");
        logger.info("priorityQueue.add(\"Friday\");");
        priorityQueue.add("Friday");
        logger.info("priorityQueue.add(\"Saturday\");");
        priorityQueue.add("Saturday");
        logger.info("priorityQueue.add(\"Sunday\");");
        priorityQueue.add("Sunday");
        logger.info("We created an Priority Queue:");
        logger.info(priorityQueue);
        logger.info("Call peek function:");
        logger.info(priorityQueue.peek());
        logger.info("Now our Priority Queue is:");
        logger.info(priorityQueue);
        logger.info("Call poll function:");
        logger.info(priorityQueue.poll());
        logger.info("Now our Priority Queue is:");
        logger.info(priorityQueue);
        logger.info("Let`s try it again:");
        logger.info(priorityQueue.poll());
        logger.info("Now our Priority Queue is:");
        logger.info(priorityQueue);
    }

    @Override
    public void task0AddStringsToListIntegers() {
        IntegerList.testList();
    }

    @Override
    public void task1ALogicTask() {
        TaskA<String> stringTaskA = new TaskA<>();
        stringTaskA.testTaskAa();
        stringTaskA.toStringTaskAa();
    }

    @Override
    public void task1BLogicTask() {
        TaskB taskB = new TaskB();
        taskB.testTask();
    }

    @Override
    public void task1CLogicTask() {
        TaskC taskC = new TaskC();
        taskC.testTask();
    }

    @Override
    public void task1DGame() {
        Game game = new Game();
        game.playGame();
    }

    @Override
    public void task2() {

    }

    @Override
    public void task3() {

    }

    @Override
    public void task4() {
        Dequeue dequeue = new Dequeue(5);
        logger.info("Insert element to end  : 5 ");
        dequeue.offerLast(5);
        logger.info("Insert element to end : 10 ");
        dequeue.offerLast(10);
        logger.info("Get last element : " + dequeue.peekLast());
        logger.info("Remove element from end :");
        dequeue.pollLast();
        logger.info("Now last is : " + dequeue.peekLast());
        logger.info("Inserting element at front");
        dequeue.offerFirst(15);
        logger.info("Get front element: " + dequeue.peekFirst());
        logger.info("Remove element at front :");
        dequeue.pollFirst();
        logger.info("Now front element : " + dequeue.peekFirst());
    }

    @Override
    public void exit() {
        System.exit('0');
    }
}
