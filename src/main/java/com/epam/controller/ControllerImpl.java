package com.epam.controller;

import com.epam.model.Model;

import static com.epam.Application.logger;


public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl(Model model) {
        this.model = model;
        logger.info("InModelConstructor");
    }

    @Override
    public void task0CustomContainer() {
        model.task0CustomContainer();
    }

    @Override
    public void task0CustomPriorityQueue() {
        model.task0CustomPriorityQueue();
    }

    @Override
    public void task0AddStringsToListIntegers() {
        model.task0AddStringsToListIntegers();
    }

    @Override
    public void task1ALogicTask() {
        model.task1ALogicTask();
    }

    @Override
    public void task1BLogicTask() {
        model.task1BLogicTask();
    }

    @Override
    public void task1CLogicTask() {
        model.task1CLogicTask();
    }

    @Override
    public void task1DGame() {
        model.task1DGame();
    }

    @Override
    public void task2() {
        model.task2();
    }

    @Override
    public void task3() {
        model.task3();
    }

    @Override
    public void task4() {
        model.task4();
    }

    @Override
    public void exit() {
        model.exit();
    }
}
