package com.epam.view;

import com.epam.controller.Controller;

import java.util.*;

import static com.epam.Application.logger;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private Controller controller;

    public MyView(Controller controller) {
        this.controller = controller;
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Test custom container, ");
        menu.put("2", "2 - Test Priority Queue");
        menu.put("3", "3 - Test addition of strings into List<Integer>");
        menu.put("4", "4 - Task A in Array sub-task");
        menu.put("5", "5 - Task B in Array sub-task");
        menu.put("6", "6 - Task C in Array sub-task");
        menu.put("7", "7 - Task D in Array sub-task");
        menu.put("8", "8 - Test container that encapsulates an array of String....");
        menu.put("9", "9 - Test class comparison");
        menu.put("10", "10 - Test custom Dequeue");
        menu.put("Q", "Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::task0CustomContainer);
        methodsMenu.put("2", this::task0CustomPriorityQueue);
        methodsMenu.put("3", this::task0AddStringsToListIntegers);
        methodsMenu.put("4", this::task1ALogicTask);
        methodsMenu.put("5", this::task1BLogicTask);
        methodsMenu.put("6", this::task1CLogicTask);
        methodsMenu.put("7", this::task1DGame);
        methodsMenu.put("8", this::task2);
        methodsMenu.put("9", this::task3);
        methodsMenu.put("10", this::task4);
        methodsMenu.put("Q", this::exit);
    }

    private void task0CustomContainer() {
        controller.task0CustomContainer();
    }

    private void task0CustomPriorityQueue() {
        controller.task0CustomPriorityQueue();
    }

    private void task0AddStringsToListIntegers() {
        controller.task0AddStringsToListIntegers();
    }

    private void task1ALogicTask() {
        controller.task1ALogicTask();
    }

    private void task1BLogicTask() {
        controller.task1BLogicTask();
    }

    private void task1CLogicTask() {
        controller.task1CLogicTask();
    }

    private void task1DGame() {
        controller.task1DGame();
    }

    private void task2() {
        controller.task2();
    }

    private void task3() {
        controller.task3();
    }

    private void task4() {
        controller.task4();
    }

    private void exit() {
        controller.exit();
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            this.outputMenu();
            logger.info("Press, number or key < Q >");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.error("ERROR. " + e);
            }
        }
        while (!keyMenu.equals("Q"));
    }
}
